#include <stm32l4xx.h>

#include <stm32l4xx_ll_rcc.h>
#include <stm32l4xx_ll_gpio.h>
#include <stm32l4xx_ll_bus.h>

void led_conf(void);
void dummy_wait(uint32_t wait);

void config_MSI(void) {
  
  // 1. Select proper clock frequency
  LL_RCC_MSI_SetRange(LL_RCC_MSIRANGE_7);
  
  // 2. Frequency value selected from RCC->CR[MSIRANGE]
  LL_RCC_MSI_EnableRangeSelection();
  
  // 3. Enable MSI clock
  LL_RCC_MSI_Enable();
  
  // 4. Wait for clock
  while(!LL_RCC_MSI_IsReady());
  
  // 5. Set MSI as clock
  LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_MSI);
  
  // 6. Do not forget to disable unused clock
  // LL_RCC_HSI_Disable(); // For example
}

int main(void) {
  led_conf();
 
  uint32_t i = 0;
  while (1) {
    if (i == 25) {
      config_MSI();
    }
    
    dummy_wait(100000);
    LL_GPIO_TogglePin(GPIOE, LL_GPIO_PIN_8);
    ++i;
  }
}

void led_conf(void) {
  LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOE);
  
  LL_GPIO_InitTypeDef led;
  led.Pin          = LL_GPIO_PIN_8;
  led.Mode         = LL_GPIO_MODE_OUTPUT;
  led.Speed        = LL_GPIO_SPEED_FREQ_LOW;
  led.OutputType   = LL_GPIO_OUTPUT_PUSHPULL;
  led.Pull         = LL_GPIO_PULL_NO;
  led.Alternate    = LL_GPIO_AF_0;

  LL_GPIO_Init(GPIOE, &led);  
}

void dummy_wait(uint32_t wait) {
  for (uint32_t i = 0; i < wait; ++i);
}
