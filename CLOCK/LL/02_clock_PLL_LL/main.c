#include <stm32l4xx.h>

#include <stm32l4xx_ll_bus.h>
#include <stm32l4xx_ll_rcc.h>
#include <stm32l4xx_ll_gpio.h>

void led_conf(void);
void dummy_wait(uint32_t wait);

void config_PLL(void) {
  
  // To understand what is going on, see clock tree from Reference Manual
  
  // 1. Set proper PLL source clock (here MSI)
  // 2. Set main division factor M
  // 3. Set multiplication factor N
  // 4. Set division factor R
  LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_MSI, LL_RCC_PLLM_DIV_1, 16, LL_RCC_PLLR_DIV_4);
  
  // 5. Enable PLL output
  LL_RCC_PLL_EnableDomain_SYS();
  
  // 6. Enable PLL clock
  LL_RCC_PLL_Enable();
  
  // 7. Wait for the clock
  while (!(LL_RCC_PLL_IsReady()));
  
  // 8. Switch clock to PLL
  LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);
}

int main(void) {
  led_conf();
  
  uint32_t i = 0;
  while (1) {
    if (i == 25) {
      config_PLL();
    }
    
    dummy_wait(100000);
    LL_GPIO_TogglePin(GPIOE, LL_GPIO_PIN_8);
    ++i;
  }
}

void led_conf(void) {
  LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOE);
  
  LL_GPIO_InitTypeDef led;
  led.Pin          = LL_GPIO_PIN_8;
  led.Mode         = LL_GPIO_MODE_OUTPUT;
  led.Speed        = LL_GPIO_SPEED_FREQ_LOW;
  led.OutputType   = LL_GPIO_OUTPUT_PUSHPULL;
  led.Pull         = LL_GPIO_PULL_NO;
  led.Alternate    = LL_GPIO_AF_0;
  
  LL_GPIO_Init(GPIOE, &led);
  LL_GPIO_SetOutputPin(GPIOE, LL_GPIO_PIN_8);
}

void dummy_wait(uint32_t wait) {
  for (uint32_t i = 0; i < wait; ++i);
}