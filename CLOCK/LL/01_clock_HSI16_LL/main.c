#include <stm32l4xx.h>

#include <stm32l4xx_ll_rcc.h>
#include <stm32l4xx_ll_bus.h>
#include <stm32l4xx_ll_gpio.h>

void led_conf(void);
void dummy_wait(uint32_t wait);

void config_HSI16(void) {
  // 1. Enable HSI16
  LL_RCC_HSI_Enable();
  
  // 2. Wait for the clock
  while (!LL_RCC_HSI_IsReady());
  
  // 3. Switch to HSI16
  LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_HSI);
  
  // 4. Do not forget to switch off unused clock
  LL_RCC_MSI_Disable();
}

int main(void) {
  led_conf();
  
  uint32_t i = 0;
  while (1) {
    if (i == 25) {
      config_HSI16();
    }
    
    dummy_wait(100000);
    LL_GPIO_TogglePin(GPIOE, LL_GPIO_PIN_8);
    ++i;
  }
}

void led_conf(void) {
  LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOE);
  
  LL_GPIO_InitTypeDef led;
  led.Pin          = LL_GPIO_PIN_8;
  led.Mode         = LL_GPIO_MODE_OUTPUT;
  led.Speed        = LL_GPIO_SPEED_FREQ_LOW;
  led.OutputType   = LL_GPIO_OUTPUT_PUSHPULL;
  led.Alternate    = LL_GPIO_AF_0;
  
  LL_GPIO_Init(GPIOE, &led);
  LL_GPIO_SetOutputPin(GPIOE, LL_GPIO_PIN_8);
}

void dummy_wait(uint32_t wait) {
  for (uint32_t i = 0; i < wait; ++i);
}
