#include <stm32l4xx.h>

void led_conf(void);
void led_toggle(void);
void dummy_wait(uint32_t wait);

void config_PLL(void) {
  
  // To understand what is going on, see clock tree from Reference Manual
  
  // 1. Set proper PLL source clock (here MSI)
  RCC->PLLCFGR = (RCC->PLLCFGR & ~(RCC_PLLCFGR_PLLSRC_Msk)) | RCC_PLLCFGR_PLLSRC_MSI;
  
  // 1. Set Main division factor
  RCC->PLLCFGR &= ~(RCC_PLLCFGR_PLLM_Msk);
  
  // 3. Set PLLCLK division factor
  RCC->PLLCFGR |= RCC_PLLCFGR_PLLR_1;
  
  // 4. Set desired frequency
  RCC->PLLCFGR = (RCC->PLLCFGR & ~(RCC_PLLCFGR_PLLN_Msk)) | (0b010000 << RCC_PLLCFGR_PLLN_Pos);
  
  // 5. Enable PLL output
  RCC->PLLCFGR |= (RCC_PLLCFGR_PLLREN);
  
  // 6. Enable PLL clock
  RCC->CR |= (RCC_CR_PLLON);
  
  // 7. Wait for the clock
  while (!(RCC->CR & RCC_CR_PLLRDY));
  
  // 8. Change clock to PLL
  RCC->CFGR = (RCC->CFGR & ~(RCC_CFGR_SW)) | (RCC_CFGR_SW_0 | RCC_CFGR_SW_1);
  
  // 9. Do not forget to switch off unused clocks (not here)
}

int main(void) {
  led_conf();
  
  uint32_t i = 0;
  while (1) {
    if (i == 25) {
      config_PLL();
    }
    
    dummy_wait(100000);
    led_toggle();
    ++i;
  }
}

void led_conf(void) {
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIOEEN;
  GPIOE->MODER = (GPIOE->MODER & ~(GPIO_MODER_MODE8)) | GPIO_MODER_MODE8_0;
  GPIOE->ODR |= GPIO_ODR_OD8;  
}

void led_toggle(void) {
  GPIOE->ODR ^= GPIO_ODR_OD8;
}

void dummy_wait(uint32_t wait) {
  for (uint32_t i = 0; i < wait; ++i);
}
