#include <stm32l4xx.h>

void led_conf(void);
void led_toggle(void);
void dummy_wait(uint32_t wait);

void config_MSI(void) {
  // 1. Select proper clock frequency
  RCC->CR = (RCC->CR & ~(RCC_CR_MSIRANGE_Msk)) | (0b1000 << RCC_CR_MSIRANGE_Pos);
  
  // 2. Frequency value selected from MSIRIGANGE in RCC->CR
  RCC->CR |= RCC_CR_MSIRGSEL;
  
  // 3. Enable clock
  RCC->CR |= RCC_CR_MSION;
  
  // 4. Wait for clock
  while (!(RCC->CR & RCC_CR_MSIRDY));
  
  // 5. Switch to the clock
  RCC->CFGR = (RCC->CFGR & ~(RCC_CFGR_SW_Msk));
  
  /* Do not forget to disable unused clock */
  // RCC->CR &= ~(RCC_CR_HSION); // For example
}

int main(void) {
  // For led
  led_conf();
  
  uint32_t i = 0;
  while (1) {
    if (i == 25) {
      config_MSI();
    }
    
    led_toggle();
    dummy_wait(100000);
    ++i;
  }
}

void led_conf(void) {
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIOEEN;
  GPIOE->MODER = (GPIOE->MODER & ~(GPIO_MODER_MODE8_Msk)) | GPIO_MODER_MODE8_0;
  GPIOE->ODR |= GPIO_ODR_OD8;
}

void led_toggle(void) {
  GPIOE->ODR ^= GPIO_ODR_OD8;
}

void dummy_wait(uint32_t wait) {
  for (uint32_t i = 0; i < wait; ++i);
}
