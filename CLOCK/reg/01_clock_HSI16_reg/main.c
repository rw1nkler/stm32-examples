#include <stm32l4xx.h>

void led_conf(void);
void led_toggle(void);
void dummy_wait(uint32_t wait);

void config_HSI16(void) {
  // 1. Enable HSI16
  RCC->CR |= RCC_CR_HSION;
  
  // 2. Wait for the clock
  while (!(RCC->CR & RCC_CR_HSIRDY));
  
  // 3. Switch to HSI16
  RCC->CFGR = (RCC->CFGR & ~(RCC_CFGR_SW_Msk)) | RCC_CFGR_SW_0;
  
  // 4. Do not forget to switch off unused clock
  RCC->CR &= ~(RCC_CR_MSION);
}

int main(void) {
  // For led
  led_conf();
  
  uint32_t i = 0;
  while (1) {
    if (i == 25) {
      config_HSI16();
    }
    
    dummy_wait(100000);
    led_toggle();
    ++i;
  }
}

void led_conf(void) {
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIOEEN;
  GPIOE->MODER = (GPIOE->MODER & ~(GPIO_MODER_MODE8_Msk)) | GPIO_MODER_MODE8_0;
  GPIOE->ODR |= GPIO_ODR_OD8;
}

void led_toggle(void) {
  GPIOE->ODR ^= GPIO_ODR_OD8;
}

void dummy_wait(uint32_t wait) {
  for (uint32_t i = 0; i < wait; ++i);
}
