#include <stm32l4xx_ll_bus.h>
#include <stm32l4xx_ll_gpio.h>

void led_conf(void);
void dummy_wait(uint32_t wait);

int main(void) {
  
  // 1. Set clock for GPIO port
  LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOA);
  
  // 2. Create GPIO Init Structure
  LL_GPIO_InitTypeDef gpio;
  
  // 3. Fill the Structure
  gpio.Pin        = LL_GPIO_PIN_0;
  gpio.Mode       = LL_GPIO_MODE_INPUT;
  gpio.Speed      = LL_GPIO_SPEED_FREQ_LOW;
  gpio.OutputType = LL_GPIO_OUTPUT_OPENDRAIN;  // Doesn't matter (type input)
  gpio.Pull       = LL_GPIO_PULL_UP;
  gpio.Alternate  = LL_GPIO_AF_0;              // Doesn't matter (type input)
  
  // 4. Configure GPIO
  LL_GPIO_Init(GPIOA, &gpio);
  
  // For led
  led_conf();
  while (1) {
    if (LL_GPIO_IsInputPinSet(GPIOA, LL_GPIO_PIN_0)) {
      LL_GPIO_TogglePin(GPIOE, LL_GPIO_PIN_8);
    }
    
  dummy_wait(100000);
  }
}

void led_conf(void) {
  LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOE);
  
  LL_GPIO_InitTypeDef led;
  led.Pin           = LL_GPIO_PIN_8;
  led.Mode          = LL_GPIO_MODE_OUTPUT;
  led.Speed         = LL_GPIO_SPEED_FREQ_LOW;
  led.OutputType    = LL_GPIO_OUTPUT_PUSHPULL;
  led.Pull          = LL_GPIO_PULL_NO;
  led.Alternate     = LL_GPIO_AF_0;
  
  LL_GPIO_Init(GPIOE, &led);
  LL_GPIO_SetOutputPin(GPIOE, LL_GPIO_PIN_8);
}

void dummy_wait(uint32_t wait) {
  for (uint32_t i = 0; i < wait; ++i);
}
