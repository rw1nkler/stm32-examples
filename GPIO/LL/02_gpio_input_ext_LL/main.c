#include <stm32l4xx.h>

#include <stm32l4xx_ll_gpio.h>
#include <stm32l4xx_ll_bus.h>
#include <stm32l4xx_ll_system.h>
#include <stm32l4xx_ll_exti.h>

void led_conf(void);

int main(void) {
  
  // 1. Set clock for GPIO port
  LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOA);
  
  // 2. Create GPIO Init Structure
  LL_GPIO_InitTypeDef gpio;
  
  // 3. Fill the structure
  gpio.Pin          = LL_GPIO_PIN_0;
  gpio.Mode         = LL_GPIO_MODE_INPUT;
  gpio.Speed        = LL_GPIO_SPEED_FREQ_LOW;
  gpio.OutputType   = LL_GPIO_OUTPUT_PUSHPULL;  // Doesn't matter
  gpio.Pull         = LL_GPIO_PULL_UP;
  gpio.Alternate    = LL_GPIO_AF_0;             // Doesn't matter
  
  // 4. Initialize GPIO
  LL_GPIO_Init(GPIOA, &gpio);
  
  // 5. Enable clock for SYSCFG
  LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_SYSCFG);
  
  // 6. Switch internal multiplexer to EXT from input GPIO0
  LL_SYSCFG_SetEXTISource(LL_SYSCFG_EXTI_PORTA, LL_SYSCFG_EXTI_LINE0);
  
  // 7. Unmask EXTI interrupt
  LL_EXTI_EnableIT_0_31(LL_EXTI_LINE_0);
  
  // 8. Set external interrupt trigered by rising edge
  LL_EXTI_EnableFallingTrig_0_31(LL_EXTI_LINE_0);
  
  // 9. Enable EXTI interrupt in NVIC
  NVIC_EnableIRQ(EXTI0_IRQn);
  
  // For led
  led_conf();
  
  while(1);
}

void EXTI0_IRQHandler(void) {
  // 10. Clear interrupt flag
  LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_0);
  
  LL_GPIO_TogglePin(GPIOE, LL_GPIO_PIN_8);
}

void led_conf(void) {
  LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOE);
  
  LL_GPIO_InitTypeDef led;
  led.Pin           = LL_GPIO_PIN_8;
  led.Mode          = LL_GPIO_MODE_OUTPUT;
  led.Speed         = LL_GPIO_SPEED_FREQ_LOW;
  led.OutputType    = LL_GPIO_OUTPUT_PUSHPULL;
  led.Pull          = LL_GPIO_PULL_NO;
  led.Alternate     = LL_GPIO_AF_0;
  
  LL_GPIO_Init(GPIOE, &led);
  LL_GPIO_SetOutputPin(GPIOE, LL_GPIO_PIN_8);
}
