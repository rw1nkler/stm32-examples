/* 

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  1. You NEED TO define in preprocessor (directry to compiler):
  USE_FULL_LL_DRIVER
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
*/

#include <stm32l476xx.h>
#include <stm32l4xx_ll_bus.h>
#include <stm32l4xx_ll_rcc.h>
#include <stm32l4xx_ll_gpio.h>

int main(void) {
  
  // 2. Enable clock for GPIO port
  LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOE);

  // 3. Declare a Init GPIO Struct
  LL_GPIO_InitTypeDef gpio;
  
  // 3. Fill the Init Struct
  gpio.Pin         = LL_GPIO_PIN_14;
  gpio.Mode        = LL_GPIO_MODE_OUTPUT;
  gpio.Speed       = LL_GPIO_SPEED_FREQ_LOW;
  gpio.OutputType  = LL_GPIO_OUTPUT_PUSHPULL;
  gpio.Pull        = LL_GPIO_PULL_NO;
  gpio.Alternate   = LL_GPIO_AF_0;             // Doesn't matter (Pin as output)
  
  // 4. Initialize Pin
  LL_GPIO_Init(GPIOE, &gpio);
  
  // 5. Set output in high level
  LL_GPIO_SetOutputPin(GPIOE, LL_GPIO_PIN_14);
}

