#include <stm32l4xx.h>

void led_conf(void);
void led_toggle(void);
void dummy_wait(uint32_t wait);

int main(void) {
  
  // 1. Enable clock for GPIO port
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIOAEN;
  
  // 2. Configure pin as input
  GPIOA->MODER &= ~(GPIO_MODER_MODE0_0 | GPIO_MODER_MODE0_1);   // MODE: 0b00;

  // 3. Set pull-up for pin
  GPIOA->PUPDR |= (GPIOA->PUPDR & ~GPIO_PUPDR_PUPD0_Msk) | (GPIO_PUPDR_PUPD0_1);  // PUPD[15:0][1:0] = 0b01;
  
  // For led
  led_conf();
  led_toggle();
  
  while (1) {
    
    // 4. Read pin
    if (GPIOA->IDR & GPIO_IDR_ID0) {
      led_toggle();
    }
    
    dummy_wait(100000);
  }
}

void led_conf(void) {
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIOEEN;
  GPIOE->MODER = (GPIOE->MODER & ~GPIO_MODER_MODE8_Msk) | GPIO_MODER_MODE8_0;
}

void led_toggle(void) {
  GPIOE->ODR ^= GPIO_ODR_OD8;
}

void dummy_wait(uint32_t wait) {
  for (uint32_t i = 0; i < wait; ++i);
}
