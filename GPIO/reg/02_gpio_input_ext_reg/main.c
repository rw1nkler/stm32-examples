#include <stm32l4xx.h>

void led_conf(void);
void led_toggle(void);

int main(void) {
  
  // 1. Set clock for GPIO port
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIOAEN;
  
  // 2. Set pin as input
  GPIOA->MODER = (GPIOA->MODER & ~(GPIO_MODER_MODE0_Msk));
  
  // 3. Set clock for SYS_CFGEN
  RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;
  
  // 4. Enable multiplexer for EXTI line
  SYSCFG->EXTICR[0] &= SYSCFG_EXTICR1_EXTI0_Msk;

  // 5. Unmask interrupt
  EXTI->IMR1 |= (EXTI_IMR1_IM0);
  
  // 6. Select interrupt on rising edge
  EXTI->RTSR1 |= EXTI_RTSR1_RT0;

  // 7. Enable interrupt in NVIC
  NVIC_EnableIRQ(EXTI0_IRQn);
  
  // For led
  led_conf();
  while (1);
}

void EXTI0_IRQHandler(void) {
  // 8. Clear interrupt flag
  EXTI->PR1 |= (EXTI_PR1_PIF0);
  
  led_toggle();
}

void led_conf(void) {
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIOEEN;
  GPIOE->MODER = (GPIOE->MODER & ~(GPIO_MODER_MODE8_Msk)) | GPIO_MODER_MODE8_0;
  GPIOE->ODR |= GPIO_ODR_OD8;
}

void led_toggle(void) {
  GPIOE->ODR ^= GPIO_ODR_OD8;
}
