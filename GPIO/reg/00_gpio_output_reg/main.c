/*
 *
 *
 */

#include <stm32l4xx.h>

void GPIO_reg(void) {
  
  // 1. Enable clock for GPIO port
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIOEEN;
    
  // 2. Configure pin as output
  GPIOE->MODER = GPIO_MODER_MODE14_0;     // Set PE14 as output MODER->MODE14[1:0] = 0b01
  
  // 3. Set high level on pin
  GPIOE->ODR = GPIO_ODR_OD14;
}

/*void GPIO_LL(void) {
  
}*/

int main(void) {
  
  /* Registers */
  GPIO_reg();
  
  /* Low Level Drivers - LL */ 
  while(1);
}
