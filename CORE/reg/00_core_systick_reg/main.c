#include <stm32l4xx.h>

void led_conf(void);
void led_toggle(void);

void systick_delay(uint32_t ms) {
  
  // 1. Program reload value
  SysTick->LOAD = 4000 * ms;
  
  // 2. Clear current value
  SysTick->VAL = 0;
  
  // 3. Select clocksource
  SysTick->CTRL |= SysTick_CTRL_CLKSOURCE_Msk;
  
  // 4. Enable SysTick
  SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;
  
  // 5. Wait for the flag
  while(!(SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk));
}

int main(void) {
  led_conf();
  
  while (1) {
    systick_delay(500);
    led_toggle();
  }
}

void led_conf(void) {
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIOEEN;
  GPIOE->MODER = (GPIOE->MODER & ~(GPIO_MODER_MODE8_Msk)) | GPIO_MODER_MODE8_0;
  GPIOE->ODR |= GPIO_ODR_OD8;  
}

void led_toggle(void) {
  GPIOE->ODR ^= GPIO_ODR_OD8;
}
