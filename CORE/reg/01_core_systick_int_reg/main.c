#include <stm32l4xx.h>

void led_conf(void);
void led_toggle(void);

int main(void) {
  led_conf();
  
  // 1. Set preload value for SysTick
  SysTick->LOAD = 4000 * 1000;
  
  // 2. Clear current value of SysTick
  SysTick->VAL = 0;
  
  // 3. Set proper clock source
  SysTick->CTRL |= SysTick_CTRL_CLKSOURCE_Msk;
  
  // 4. Enable SysTick interrupts
  SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk;
  
  // 5. Enable SysTick
  SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;
  
  while (1);
}

void SysTick_Handler(void) {
  // 6. Fill SysTick handler
  led_toggle();
}

void led_conf(void) {
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIOEEN;
  GPIOE->MODER = (GPIOE->MODER & ~(GPIO_MODER_MODE8_Msk)) | GPIO_MODER_MODE8_0;
  GPIOE->ODR |= GPIO_ODR_OD8;
}

void led_toggle(void) {
  GPIOE->ODR ^= GPIO_ODR_OD8;
}