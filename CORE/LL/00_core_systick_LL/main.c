#include <stm32l4xx.h>

#include <stm32l4xx_ll_cortex.h>
#include <stm32l4xx_ll_bus.h>
#include <stm32l4xx_ll_gpio.h>

void led_conf(void);

void systick_delay(uint32_t ms) {
  
  // 1. Program reload value (Impossible with LL)
  SysTick->LOAD = 4000 *ms;
  
  // 2. Clear current value
  SysTick->VAL = 0;
  
  // 3. Select clock source
  LL_SYSTICK_SetClkSource(LL_SYSTICK_CLKSOURCE_HCLK);
  
  // 4. Enable SysTick
  SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;
  
  // 5. Wait for flag
  while (!(LL_SYSTICK_IsActiveCounterFlag()));
}

int main(void) {
  led_conf();
  
  while (1) {
    systick_delay(500);
    LL_GPIO_TogglePin(GPIOE, LL_GPIO_PIN_8);
  }
}

void led_conf(void) {
  LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOE);
  
  LL_GPIO_InitTypeDef led;
  led.Pin           = LL_GPIO_PIN_8;
  led.Mode          = LL_GPIO_MODE_OUTPUT;
  led.Speed         = LL_GPIO_SPEED_FREQ_LOW;
  led.OutputType    = LL_GPIO_OUTPUT_PUSHPULL;
  led.Pull          = LL_GPIO_PULL_NO;
  led.Alternate     = LL_GPIO_AF_0;
  
  LL_GPIO_Init(GPIOE, &led);
  LL_GPIO_SetOutputPin(GPIOE, LL_GPIO_PIN_8);
}
