#include <stm32l4xx.h>

/* --- L3GD20 gyroscope registers --- */

#define WHO_AM_I   0x0F
#define CTRL1      0x20
#define STATUS     0x27
#define OUT_X_L    0x28
#define OUT_X_H    0x29
#define OUT_Y_L    0x2A
#define OUT_Y_H    0x2B
#define OUT_Z_L    0x2C
#define OUT_Z_H    0x2D

#define WHO_AM_I_VAL 0xD4
#define STATUS_ZYXDA (1 << 3)

void led_config(void);
void sys_wait(uint32_t wait);

int32_t lambda = 80;

int32_t x_axis = 0;
int32_t y_axis = 0;
int32_t z_axis = 0;

double x_angle = 0.0;
double y_angle = 0.0;
double z_angle = 0.0;

char SPI_read(char addr) {
  uint16_t msg = (0x80 | addr);
  msg = ((msg << 8) | 0x00FF);
  
  // 1. Read DR register to clear TXE and RXNE
  uint16_t ret = SPI2->DR;
  
  // 2. Pull-down CS
  GPIOD->ODR &= ~(GPIO_ODR_OD7);
  sys_wait(1);
  
  // 3. Write the message
  SPI2->DR = msg;
  
  // 4. Wait for flags
  while (!(SPI2->SR & SPI_SR_TXE));
  while (!(SPI2->SR & SPI_SR_RXNE));
  
  // 5. Read answer (here communication is 8-bit, so only half of DR is needed)
  ret = (0x00FF & SPI2->DR);
  
  // 6. Pull-up CS
  GPIOD->ODR |= GPIO_ODR_OD7;
  sys_wait(1);
 
  return ret;
}

void SPI_write(char addr, char val) {
  uint16_t msg = ((addr << 8) | val);
  
  // 1. Read DR register to clear TXE and RXNE
  uint16_t ret = SPI2->DR;
  
  // 2. Pull-down CS
  GPIOD->ODR &= ~(GPIO_ODR_OD7);
  sys_wait(1);
  
  // 3. Write the message
  SPI2->DR = msg;
  
  // 4. Wait for flags
  while (!(SPI2->SR & SPI_SR_TXE));
  while (!(SPI2->SR & SPI_SR_RXNE));
  
  // 5. Pull-up CS
  GPIOD->ODR |= GPIO_ODR_OD7;
  sys_wait(1);
}

void SPI_config(void) {
  
  // 1. Configure clock for GPIO port (of SPI2)
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIODEN;    // Clock for GPIOD
  
  // 2. Set proper modes for GPIOs
  GPIOD->MODER = (GPIOD->MODER & ~GPIO_MODER_MODE1_Msk) | (GPIO_MODER_MODE1_1);   // PD1 (SCK)   - alternate function
  GPIOD->MODER = (GPIOD->MODER & ~GPIO_MODER_MODE3_Msk) | (GPIO_MODER_MODE3_1);   // PD3 (MISO)  - alternate function
  GPIOD->MODER = (GPIOD->MODER & ~GPIO_MODER_MODE4_Msk) | (GPIO_MODER_MODE4_1);   // PD4 (MOSI)  - alternate function
  GPIOD->MODER = (GPIOD->MODER & ~GPIO_MODER_MODE7_Msk) | (GPIO_MODER_MODE7_0);   // PD7 (CS)    - output
  
  // 3. Select adequate alternate functions
  GPIOD->AFR[0] |= (GPIO_AFRL_AFSEL1_0 | GPIO_AFRL_AFSEL1_2);                     // PD0 - alternate AF5 (SCK)
  GPIOD->AFR[0] |= (GPIO_AFRL_AFSEL3_0 | GPIO_AFRL_AFSEL3_2);                     // PD3 - alternate AF5 (MISO)
  GPIOD->AFR[0] |= (GPIO_AFRL_AFSEL4_0 | GPIO_AFRL_AFSEL4_2);                     // PD4 - alternate AF5 (MOSI)
  
  // 4. Configure clock for SPI2
  RCC->APB1ENR1 |= RCC_APB1ENR1_SPI2EN;                                           // Clock
  
  // 5. Set Baud Rate Prescaler
  SPI2->CR1 = (SPI2->CR1 & ~(SPI_CR1_BR_Msk)) | (SPI_CR1_BR_0);                   // Baud rate clk/2 (here 4MHz/2 = 2MHz)
  
  // 6. Set transfer direction (Full-Duplex or Half-Duplex);
  SPI2->CR1 &= ~(SPI_CR1_RXONLY);                                                 // Full-duplex
  SPI2->CR1 &= ~(SPI_CR1_BIDIMODE);                                               // 2-line unidirectional data mode
  
  // 7. Select phase (CPHA) and polarity (CPOL) 
  SPI2->CR1 &= ~(SPI_CR1_CPHA);
  SPI2->CR1 &= ~(SPI_CR1_CPOL);
  
  // 8. Select Master Mode
  SPI2->CR1 |= SPI_CR1_MSTR;
  
  // 8. Set data width
  SPI2->CR2 |= (SPI_CR2_DS_0 | SPI_CR2_DS_1 | SPI_CR2_DS_2 | SPI_CR2_DS_3);        // 16-bit
  
  // 9. Set slave management options
  SPI2->CR1 |= SPI_CR1_SSM;
  SPI2->CR1 |= SPI_CR1_SSI;
  
  // 10. Enable SPI2
  SPI2->CR1 |= SPI_CR1_SPE;
  
  // 11. Make CS high
  GPIOD->ODR |= GPIO_ODR_OD7;
}

int main(void) {
  
  led_config();

  // Init other CS of the SPI2 bus
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIOEEN;
  GPIOE->MODER = (GPIOE->MODER & ~(GPIO_MODER_MODE0_Msk)) | (GPIO_MODER_MODE0_0);
  GPIOE->ODR |= GPIO_ODR_OD0;
  
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIOCEN;
  GPIOC->MODER = (GPIOC->MODER & ~(GPIO_MODER_MODE0_Msk)) | (GPIO_MODER_MODE0_0);
  GPIOC->ODR |= GPIO_ODR_OD0;

  // Init SPI2
  SPI_config();
  
  // Check if gyroscope is responding
  uint16_t who_am_i = SPI_read(WHO_AM_I);
  if (who_am_i == WHO_AM_I_VAL) {
    GPIOE->ODR |= GPIO_ODR_OD8;
  }
  
  // Activate gyroscope
  SPI_write(CTRL1, 0x0F);
  uint32_t read = SPI_read(CTRL1);
  if (read == 0x0F) {
    GPIOB->ODR |= GPIO_ODR_OD2;
  }   
  
  sys_wait(1000);
  GPIOE->ODR &= ~(GPIO_ODR_OD8);
  GPIOB->ODR &= ~(GPIO_ODR_OD2);
  
  uint8_t status;
  uint8_t lsb, msb;
  int16_t whole;
  
  double prev_x_dps = 0.0;
  double curr_x_dps = 0.0;
  double prev_y_dps = 0.0;
  double curr_y_dps = 0.0;
  double prev_z_dps = 0.0;
  double curr_z_dps = 0.0;
  uint32_t curr_time = 0, prev_time = 0;
  
  while(1) {
    status = SPI_read(STATUS);
    if (status & STATUS_ZYXDA) {
      
      lsb = SPI_read(OUT_X_L);
      msb = SPI_read(OUT_X_H);
      whole = (msb << 8) | lsb;
      x_axis = ((100L - lambda)*x_axis + lambda*whole)/100L;
      
      curr_x_dps = (x_axis * 245.0) / 32767.0;
      x_angle += (prev_x_dps + curr_x_dps)*((curr_time - prev_time)*0.05)/2.0;
      prev_x_dps = curr_x_dps;
      
      
      lsb = SPI_read(OUT_Y_L);
      msb = SPI_read(OUT_Y_H);
      whole = (msb << 8) | lsb;
      y_axis = ((100L - lambda)*y_axis + lambda*whole)/100L;
      
      curr_y_dps = (y_axis * 245.0) / 32767.0;
      y_angle += (prev_y_dps + curr_y_dps)*((curr_time - prev_time)*0.05)/2.0;
      prev_y_dps = curr_y_dps;
    
      
      lsb = SPI_read(OUT_Z_L);
      msb = SPI_read(OUT_Z_H);
      whole = (msb << 8) | lsb;
      z_axis = ((100L - lambda)*z_axis + lambda*whole)/100L;
      
      curr_z_dps = (z_axis * 245.0) / 32767.0;
      z_angle += (prev_z_dps + curr_z_dps)*((curr_time - prev_time)*0.05)/2.0;
      prev_z_dps = curr_z_dps;
      
      prev_time = curr_time;
    }
    
    sys_wait(50);
    ++curr_time;
    GPIOB->ODR ^= GPIO_ODR_OD2;
  }
}
  
void led_config(void) {
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIOEEN;
  GPIOE->MODER = (GPIOE->MODER & ~(GPIO_MODER_MODE8_Msk)) | GPIO_MODER_MODE8_0;
  
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIOBEN;
  GPIOB->MODER = (GPIOB->MODER & ~(GPIO_MODER_MODE2_Msk)) | GPIO_MODER_MODE2_0;
}

void sys_wait(uint32_t ms) {
  SysTick->LOAD = 4000 * ms;
  SysTick->VAL = 0;
  SysTick->CTRL |= SysTick_CTRL_CLKSOURCE_Msk;
  SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;
  while (!(SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk));
}